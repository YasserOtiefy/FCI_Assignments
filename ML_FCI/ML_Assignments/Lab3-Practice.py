
import pandas
import matplotlib.pyplot as plt
from pandas.tools.plotting import scatter_matrix



def gradient_descent(X, y, theta, alpha, num_iters):
    '''
    Performs gradient descent to learn theta
    by taking num_items gradient steps with learning
    rate alpha and update theata0, theata1
    '''

    return theta


#Load the dataset
data = pandas.read_csv('data.csv')

scatter_matrix(data[['population','profit']])
plt.show()

X = data['population']
y = data['profit']
#number of training samples
m = y.size
#Initialize theta parameters
theta0 = 0
theta1 =0
#Some gradient descent settings
iterations = 1500
alpha = 0.01
#compute and display initial cost
theta= gradient_descent(X, y, [theta0,theta1], alpha, iterations) #To be completed by students
print theta

#Predict values for population sizes of 3.5 and 7.0
#Students write prediction code

