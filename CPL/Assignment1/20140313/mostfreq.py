def frequentPatterns(text="", k=2):
    
    words = {}
    frequentPatterns = []

      #create a dictionary of all patterns of length k in text with their respective frequencies
    for i in range(len(text)-k + 1): #iterate over the valid length of the string
        a = text[i:i+k]
        if a in words:
              words[a] = words[a] + 1
        else:
            words[a] = 1

    largestValue=0
    for k, v in words.iteritems():
        if v > largestValue:     
            frequentPatterns = []
            frequentPatterns.append(k) 
            largestValue = v
        if v == largestValue:
            frequentPatterns.append(k)


    return set(frequentPatterns)