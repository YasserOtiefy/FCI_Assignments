import smooth_curve_fitting as GA
#----------------------------------------------------------------------------#
casses = []
class Point:
    'Point object'
    def __init__( self , x  , y ):
        self.x = x
        self.y = y
# ---------------------- get cases from the file  ---------------
file = open("input.txt", "r")
numOfCases = int(file.readline())
for iCases in range(0 , numOfCases):
    line = file.readline()
    numOfPoints, degree = line.split(' ')
    numOfPoints = int(numOfPoints)
    degree = int(degree)
    points =[]
    for point in range(0 , numOfPoints):
        p = file.readline()
        p = p.split(' ')
        points.append(Point(float(p[0]) ,float(p[1])))
    casses.append({ 'size' : line , 'points' : points })
# ------------------------------------------------------------------#
# ----------------------------- main -------------------------------#
# ------------------------------------------------------------------#
for  i , case in enumerate(casses):
    selectedChromosome = GA.select_chromosome(case['size'] , case['points'])
    print('case %s :' %(i + 1))
    print('Error %s'%(GA.get_MSError(case['points'], selectedChromosome)))
    print('Coefficients: ', end='')
    for i, c in enumerate(selectedChromosome):
        if i != len(selectedChromosome) - 1:
            print('%f' %(c), end=', ')
        else:
            print('%f' %(c), end='.')
    print('\n\n\n')
#######################################################################################
